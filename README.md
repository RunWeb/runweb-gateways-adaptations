# RunWeb Adapted Payment Gateways

This collection is based on multiple open source payment e-com providers. Collating and adapting the gateways to work within a PHP based RunWeb setup. 

Some noted gateways for UK users include; 

HSBC

Barclays

RBS Group

Lloyds Bank

SagePay

PayPal

NOTE: THIS IS A FORKING PROFILE; CODE IS NOT DIRECTLY USED FROM THESE MODULES. IT ENABLES REFERENCING FOR THE PHP USE OF BANK/GATEWAY API'S.
